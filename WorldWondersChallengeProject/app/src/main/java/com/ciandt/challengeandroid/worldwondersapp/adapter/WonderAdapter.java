package com.ciandt.challengeandroid.worldwondersapp.adapter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ciandt.challengeandroid.worldwondersapp.database.DataBaseWonder;
import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.model.WonderInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joan on 16/08/15.
 */
public class WonderAdapter extends BaseAdapter{

    private List<WonderInfo> listWonder = new ArrayList<WonderInfo>();
    private SQLiteDatabase database;
    DataBaseWonder baseWonder;
    public WonderAdapter (Context context){
        //this is example for get the information
        loadWonderInDB(context);

    }

    @Override
    public int getCount() {
        if (listWonder != null) {
            return listWonder.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getItem(int position) {
        if (listWonder != null) {
            return listWonder.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (listWonder != null) {
            final WonderInfo wonderInfo = listWonder.get(position);
            final LayoutInflater layoutInflater = (LayoutInflater) parent
                    .getContext().getSystemService(
                            Context.LAYOUT_INFLATER_SERVICE);

            View  listItemView = layoutInflater.inflate(R.layout.wondersinfo,
                    parent, false);


            TextView tvtitle = (TextView) listItemView.findViewById(R.id.tvListWonderName);
            tvtitle.setText(wonderInfo.getName());
            TextView tvcountry = (TextView) listItemView.findViewById(R.id.tvListWonderCountry);
            tvcountry.setText(wonderInfo.getCountry());
            TextView tvdetails = (TextView) listItemView.findViewById(R.id.tvListWonderDetails);
            tvdetails.setText(wonderInfo.getDescription());

            return listItemView;
        }
        return null;


    }
    //Method to load the Wonder in DB
    private void loadWonderInDB(Context context){

        baseWonder = new DataBaseWonder(context);
        database = baseWonder.getReadableDatabase();
        String[] campos =  {baseWonder.ID,baseWonder.NAME, baseWonder.COUNTRY, baseWonder.DETAILS };
        Cursor cursor;
        cursor = database.query(baseWonder.TABELA, campos, null, null, null, null, null, null);
        if(cursor!=null)
        { cursor.moveToFirst();
        }

        database.close();

        String name, country, details;
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){

            name = cursor.getString(1);
            country = cursor.getString(2);
            details = cursor.getString(3);

            WonderInfo wi = new WonderInfo(name, country, details);
            listWonder.add(wi);
            cursor.moveToNext();
        }

        cursor.close();

    }
}
