package com.ciandt.challengeandroid.worldwondersapp.model;

/**
 * Created by joan on 16/08/15.
 */
public class User {
    //User fields
    private String name;
    private String email;

    //....


    public User(String name, String email){
        this.name=name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
}
