package com.ciandt.challengeandroid.worldwondersapp.model;

/**
 * Created by joan on 16/08/15.
 */

import android.content.res.Resources;

public class WonderInfo {

    private String name;
    private String country;
    private String description;

    public WonderInfo(String name, String country, String description){
        this.name =name;
        this.country=country;
        this.description=description;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCountry() {
        return country;
    }
}
