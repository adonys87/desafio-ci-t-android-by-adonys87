package com.ciandt.challengeandroid.worldwondersapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by joan on 16/08/15.
 */
public class DataBaseWonder  extends SQLiteOpenHelper{

    private static final String DB_NAME = "wonderDB.db";
    //User Table
    public static final String TABELA_USER = "users";
    public static final String ID_USER = "_id";
    public static final String NAME_USER = "name";
    public static final String EMAIL_USER = "email";
    public static final String PASS_USER ="password";

    //Wonders Table
    public static final String TABELA = "wonders";
    public static final String ID = "_id";
    public static final String NAME = "name";
    public static final String COUNTRY = "country";
    public static final String DETAILS = "details";
    private static final int VERSION = 4;

    public DataBaseWonder(Context context) {
        super(context, DB_NAME, null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE "+TABELA+"(" +
                ID + " integer primary key autoincrement," + NAME + " text," +
                COUNTRY + " text," +
                DETAILS + " text" +")";

        db.execSQL(sql);

        sql = "CREATE TABLE "+TABELA_USER+"(" +
                ID_USER + " integer primary key autoincrement," + NAME_USER + " text," +
                EMAIL_USER + " text," +
                PASS_USER + " text)";

        db.execSQL(sql);

        //Insert example
        ContentValues values;
        values = new ContentValues();
        values.put(NAME, "Machu Pichu");
        values.put(COUNTRY, "Peru");
        values.put(DETAILS, "Machu Picchu (em quíchua Machu Pikchu, \"velha montanha\"),[1] também chamada \"cidade perdida dos Incas\",[2] é uma cidade pré-colombiana bem conservada, localizada no topo de uma montanha, a 2400 metros de altitude, no vale do rio Urubamba, atual Peru. Foi construída no século XV, sob as ordens de Pachacuti. O local é, provavelmente, o símbolo mais típico do Império Inca, quer devido à sua original localização e características geológicas, quer devido à sua descoberta tardia em 1911. Apenas cerca de 30% da cidade é de construção original, o restante foi reconstruído. As áreas reconstruídas são facilmente reconhecidas, pelo encaixe entre as pedras. A construção original é formada por pedras maiores, e com encaixes com pouco espaço entre as rochas.\n" +
                "\n" +
                "            Consta de duas grandes áreas: a agrícola formada principalmente por terraços e recintos de armazenagem de alimentos; e a outra urbana, na qual se destaca a zona sagrada com templos, praças e mausoléus reais.\n" +
                "\n" +
                "            A disposição dos prédios, a excelência do trabalho e o grande número de terraços para agricultura são impressionantes, destacando a grande capacidade daquela sociedade. No meio das montanhas, os templos, casas e cemitérios estão distribuídos de maneira organizada, abrindo ruas e aproveitando o espaço com escadarias. Segundo a história inca, tudo planejado para a passagem do deus sol.\n" +
                "\n" +
                "            O lugar foi elevado à categoria de Património mundial da UNESCO, tendo sido alvo de preocupações devido à interação com o turismo por ser um dos pontos históricos mais visitados do Peru.\n" +
                "\n" +
                "            Há diversas teorias sobre a função de Machu Picchu, e a mais aceita afirma que foi um assentamento construído com o objetivo de supervisionar a economia das regiões conquistadas e com o propósito secreto de refugiar o soberano Inca e seu séquito mais próximo, no caso de ataque.</description>\n" +
                "   ");

        db.insert(TABELA, null, values);
        values.put(NAME, "Chichen Itza");
        values.put(COUNTRY, "Mexico");
        values.put(DETAILS, "Chichén Itzá (do iucateque: Chi'ch'èen Ìitsha) é uma cidade arqueológica maia localizada no estado mexicano de Iucatã que funcionou como centro político e econômico da civilização maia. As várias estruturas – a pirâmide de Kukulkán, o Templo de Chac Mool, a Praça das Mil Colunas, e o Campo de Jogos dos Prisioneiros – podem ainda hoje ser admiradas e são demonstrativas de um extraordinário compromisso para com a composição e espaço arquitetónico.\n" +
                "        O nome Chichén-Itzá tem raiz maia e significa \"pessoas que vivem na beira da água\". Estima-se que Chichén-Itzá foi fundada por volta dos anos 435 e 455 a.C. Foi declarada Património Mundial da Unesco em 1988.\n" +
                "   ");

        db.insert(TABELA, null, values);

        ContentValues valuesUser;
        valuesUser = new ContentValues();
        valuesUser.put(NAME_USER, "Teste");
        valuesUser.put(EMAIL_USER,"teste@cit.com");
        valuesUser.put(PASS_USER,"oportunidade");
        db.insert(TABELA_USER, null, valuesUser);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABELA);
        db.execSQL("DROP TABLE IF EXISTS " + TABELA_USER);
        onCreate(db);
    }



}
