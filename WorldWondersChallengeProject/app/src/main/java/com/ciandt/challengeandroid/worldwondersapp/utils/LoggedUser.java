package com.ciandt.challengeandroid.worldwondersapp.utils;

import com.ciandt.challengeandroid.worldwondersapp.model.User;

/**
 * Created by joan on 16/08/15.
 */
public class LoggedUser {




    private static User loggedUser;

    public static boolean addLoogedUser(User u){
            // here it can create one Logged User.
            if(loggedUser == null){
                loggedUser = u;
                return true;
            }else
            {
                return false;
            }
    }

    public static boolean isUserLogged(){
        if(loggedUser == null){
            return false;
        }
        else{
            return true;
        }
    }

    public static void disconnectUser(){
        loggedUser = null;
    }

    public static String getNameLoggedUser(){
        if(loggedUser != null){
            return loggedUser.getName();
        }
        return "";
    }
    public static String getEmailLoggedUser(){
        if(loggedUser != null){
            return loggedUser.getEmail();
        }
        return "";
    }

}
