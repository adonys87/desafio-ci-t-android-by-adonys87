package com.ciandt.challengeandroid.worldwondersapp.activity;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.ciandt.challengeandroid.worldwondersapp.database.DataBaseWonder;
import com.ciandt.challengeandroid.worldwondersapp.utils.LoggedUser;
import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.model.User;


public class LoginActivity extends Activity implements View.OnClickListener{



    public static final int MIN_PASS_SIZE = 4;

    private EditText etuser;
    private EditText etpass;
    //Field for SharedPreferences
    public static final String PREF_LOGGED_USER = "loggeduser";
    private static final int MODE_SHARED_PREF =0;

    public static final String HAS_USER = "hasUser";
    public static final String NAME_USER = "nameUser";
    public static final String EMAIL_USER = "emailUser";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //check if User is logged
        if(!checkUserLogged()) {

            TextWatcher tw = initTextWatcher();

            etuser = (EditText) findViewById(R.id.etLogin);
            etpass = (EditText) findViewById(R.id.etPassword);
            Button btLogin = (Button) findViewById(R.id.btLogin);
            //Add listener for views
            etuser.addTextChangedListener(tw);
            etpass.addTextChangedListener(tw);
            btLogin.setOnClickListener(this);
        }
        else{
            callNextActivity();
        }
    }


    //check if the
    private boolean checkUserLogged() {
        boolean isUser =false;
        String name="";
        String email="";


        SharedPreferences sharedPref = getSharedPreferences(PREF_LOGGED_USER, MODE_SHARED_PREF);
        isUser = sharedPref.getBoolean(HAS_USER, false);
        if(isUser)
        {
            name = sharedPref.getString(NAME_USER, "");
            email = sharedPref.getString(EMAIL_USER, "");
            User user = new User(name, email);
            LoggedUser.addLoogedUser(user);
        }


        return isUser;
    }

    //Start the TextWatcher
    private TextWatcher initTextWatcher(){
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    callClearErrors(s);
                }
            }
        };

        return textWatcher;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @Override
    public void onClick(View view) {
        //if Button
        if(view.getId() == R.id.btLogin){
            if(validateFieldsForLogin()){
                //search the user in DB
                if(!searchUserInDB())
                    Toast.makeText(this, R.string.error_incorrect_credentials,Toast.LENGTH_LONG).show();
                else{
                    //If there is no problem, call the next Screen
                    callNextActivity();
                }
            }
        }
    }

    private void callNextActivity(){
        Toast.makeText(this,getResources().getText(R.string.hello_user)+" "+ LoggedUser.getNameLoggedUser(), Toast.LENGTH_LONG).show();
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
    }
    // access in DataBase with valid user.
    private boolean searchUserInDB() {
        User user = getUserInDB(etuser.getText().toString().trim(),
                etpass.getText().toString().trim());
        //There is no user in DB
        if(user ==null)
            return false;

        LoggedUser.addLoogedUser(user);
        saveInSharedPreference(user);
        return true;
    }

    private User getUserInDB(String name, String pass) {
        SQLiteDatabase database;
        User ret =null;
        DataBaseWonder baseWonder = new DataBaseWonder(getApplicationContext());
        database = baseWonder.getReadableDatabase();
        String[] campos =  {baseWonder.NAME_USER, baseWonder.EMAIL_USER, baseWonder.PASS_USER };
        String[] args = new String[]{name, pass};
        Cursor cursor;
        cursor = database.query(baseWonder.TABELA_USER, campos, baseWonder.EMAIL_USER +" = ? AND " +baseWonder.PASS_USER+" = ?", args, null, null, null, null);
        if(cursor!=null)
        { cursor.moveToFirst();
        }

        database.close();
        String username,email;

        if(cursor.getCount()==1)
        {
            username = cursor.getString(0);
            email = cursor.getString(1);
            ret = new User(username, email);

        }

        cursor.close();
        return ret;
    }

    private void saveInSharedPreference(User user) {
        if(user != null){
            SharedPreferences sharedPref = getSharedPreferences(PREF_LOGGED_USER, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putBoolean(HAS_USER, true);
            editor.putString(NAME_USER, user.getName());
            editor.putString(EMAIL_USER, user.getEmail());
            editor.commit();
        }
    }

    //method to validate all fields in Login Screen
    private boolean validateFieldsForLogin() {

        if(isValidEmail(etuser.getText().toString().trim()) && isValidPass(etpass.getText().toString().trim()))
                return true;
        else
            return false;
    }
    //this method will validate the email field

    private boolean isValidEmail(String email){

        if(email == null)
            return false;

        if(TextUtils.isEmpty(email))
        {
            etuser.requestFocus();
            etuser.setError(getResources().getString(R.string.error_field_required));
            return false;
        }

        if (! android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            etuser.requestFocus();
            etuser.setError(getResources().getString(R.string.error_invalid_email));

            return false;
        }
        return true;
    }
    //this method will validate the password field
    private boolean isValidPass(String password) {
        if(password == null)
            return false;

        if(TextUtils.isEmpty(password))
        {
            etpass.requestFocus();
            etpass.setError(getResources().getString(R.string.error_field_required));

            return false;
        }

        if(password.length() < MIN_PASS_SIZE)
        {
            etpass.requestFocus();
            etpass.setError(getResources().getString(R.string.error_invalid_password));
            return false;
        }

        return true;
    }

    private void callClearErrors(Editable s) {
        if (!s.toString().isEmpty()) {
               etuser.setError(null);
               etpass.setError(null);
            }
    }
    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }

}
